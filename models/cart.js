var mongoose = require("mongoose");

var cartSchema = new mongoose.Schema({
    book_id: String,
    price: String,
    quantity: {
        type: Number,
        default: 0,
    },
    buyer: {
        id:{
          type: mongoose.Schema.Types.ObjectId,
          ref: "User"
        },
        username: String
      }
  });

module.exports = mongoose.model("Cart",cartSchema);
