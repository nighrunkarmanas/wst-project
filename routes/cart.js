var express    = require("express");
var router     = express.Router();
var Book       = require("../models/book");
var Cart       = require("../models/cart")

var middleware = require("../middleware");
const cart = require("../models/cart");

router.post("/:id", middleware.isLoggedIn, function(req,res){
  // Create a new book and save to the DB
  Book.findById(req.params.id,async function(err, foundBook){
    if(err){
      console.log(err);
    }
    var temp_buyer = {
      username: req.user.username,
      id: req.user._id,
    }
    var cart = await Cart.findOne({book_id: foundBook._id, buyer: temp_buyer})
    if(cart){
      cart.quantity++;
    }
    else{
      cart = new Cart({
        book_id: foundBook._id,
        price: foundBook.price,
        quantity: 1,
        buyer:{
          username: req.user.username,
          id: req.user._id
        }
      })
    }
    cart.save()
        //redirect to cart page
        res.redirect("/cart");
    });
});

router.get("/",middleware.isLoggedIn, async function(req,res){
  // Get all books from the DB
  var temp_buyer = {
    username: req.user.username,
    id: req.user._id,
  }
  var cart = await Cart.find({buyer: temp_buyer})
  var displayCart = {items:[]};
  var total = 0;

  //Get total
  for(var item in cart){
    displayCart.items.push(cart[item]);
    total += (cart[item].quantity * cart[item].price);
  }
  displayCart.total = total;
  
  Book.findById(req.params.id,function(err, foundBook){
      if(err){
        console.log("SOMETHING GOT WRONG!");
        console.log(err);
      }
      else {
        // Render cart
        res.render("cart/index",{cart: displayCart, total: total, books: foundBook});

          }
      });
});

router.get("/delete:id",middleware.isLoggedIn, async function(req,res){
  // Get all books from the DB
  var temp_buyer = {
    username: req.user.username,
    id: req.user._id,
  }
  //console.log(temp_buyer, req.params.id.slice(1))
  var cart = await Cart.deleteOne({buyer: temp_buyer, book_id: req.params.id.slice(1)})
  res.redirect("/cart")
});

module.exports = router;
